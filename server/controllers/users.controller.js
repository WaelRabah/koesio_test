const db = require("../db/database");



exports.getUserByID= async (req,res)=>{
    const {id}=req.params
    if (req.user.id!=id){
      res.status(401).json({
        message : "Unauthorized access"
      })
      return
    }
    const { rows } = await db.query(
        "SELECT * FROM USERS where id=$1",[id]
      );
    if (!rows.length) {
      res.status(404).json({
        message : "User not found !"
      })
      return
    }
    res.status(200).json({
      message : "success",
      user:{
        ...rows[0]
      }
    })
    return
}


const db = require("../db/database");
const { sendDiscordMsg, sendSlackMsg } = require("../services/webhooks.service");

exports.getMessagesByUser = async (req, res) => {
    const { userId } = req.params
    if (req.user.id != userId) {
        res.status(401).json({
            message: "Unauthorized access !"
        })
        return
    }
    const { rows } = await db.query(
        "SELECT * FROM MESSAGES where userId=$1", [userId]
    );

    const messages = await Promise.all(rows.map(async (message) => {
        const { rows } = await db.query(
            "SELECT * FROM MESSAGEPERPLATFORM where messageId=$1", [message['id']]
        );
        return { ...message, platforms: rows }
    }))
    res.status(200).json({
        message: "success",
        results: [...messages]
    })
    return
}
async function dispatchMessage(content, platforms, fullname) {
    const statusPerPlatform = await Promise.all(platforms.map(async (p) => {
        if (p['webhookUrl'].length === 0) {
            return false
        }
        if (p['name'] === "Discord") {
            try {
                const res = await sendDiscordMsg(content, p['webhookUrl'], fullname)
            } catch (error) {
                if (error.code === "WEBHOOK_URL_INVALID") {
                    return false
                }
            }
            return true
        }
        if (p['name'] === "Slack") {
            try {
                const res = await sendSlackMsg(content, p['webhookUrl'], fullname)
                return res['text'] === 'ok'
            } catch (error) {
                if (error.code==="slack_webhook_request_error"){
                    return false
                }
            }
            
            
        }
        return false
    }))
    let status = false

    for (let i = 0; i < statusPerPlatform.length; i++) {
        status = status || statusPerPlatform[i]
    }
    return { statusPerPlatform, status }
}
exports.sendMessage = async (req, res) => {

    const { userId } = req.params
    const { content, platforms } = req.body

    if (req.user.id != req.params.userId) {
        res.status(401).json({
            message: "Unauthorized access"
        })
        return
    }
    const userDataRes = await db.query(
        "SELECT * FROM USERS WHERE id=$1;",
        [userId]
    )
    const userData = userDataRes['rows'][0]
    const { statusPerPlatform, status } = await dispatchMessage(content, platforms, userData['fullname'])
    const { rows } = await db.query(
        "INSERT INTO MESSAGES (content, status, userId) VALUES ($1, $2, $3) RETURNING *;",
        [content, status, userId]
    )
    const message = rows[0]
    const n_platforms = []
    for (let i = 0; i < platforms.length; i++) {
        const p = platforms[i]
        const status = statusPerPlatform[i]
        const { rows } = await db.query(
            "INSERT INTO MESSAGEPERPLATFORM (name, status, messageId) VALUES ($1, $2, $3) RETURNING *;",
            [p['name'], status, message['id']]
        )
        n_platforms.push({
            ...rows[0]
        })
    }
    res.status(200).json({
        message: "success",
        messageData: { ...message, platforms: [...n_platforms] }
    })
    return
}
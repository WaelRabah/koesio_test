const db = require("../db/database");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken')
const {  validationResult } = require('express-validator');
exports.signup= async (req,res)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ message:"Error",errors: errors.array() });
    }

    const {fullname, email, password}=req.body
    const res_q= await db.query(
      "SELECT * FROM USERS WHERE email=$1",
      [email]
    );
    console.log(res_q)
    if (res_q.rows.length>0){
      res.status(302).json({
          message: "Account already exists !"
        }); 
        return
    }
    const salt=await bcrypt.genSalt()
    const hash= bcrypt.hashSync( password, salt )
    const { rows } = await db.query(
        "INSERT INTO users (fullname, email, password) VALUES ($1, $2, $3)",
        [fullname, email,hash]
      )
      res.status(201).json({
        message: "User registered successfully!",
        userData: { 
          fullname, email, password:hash 
        },
      });


}

exports.login= async (req,res)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ message:"Error",errors: errors.array() });
    }
    const { email, password}=req.body
    const { rows } = await db.query(
        "SELECT * FROM USERS WHERE email=$1",
        [email]
      );
      if (rows.length==0){
        res.status(404).json({
            message: "User not found !"
          });
          return
      }
      const user=rows[0]
      const isCorrect= bcrypt.compareSync( password, user['password'] )
      
      if (!isCorrect){
        res.status(403).json({
            message: "Wrong password",
          });
          return
      }
      const payload={email:user.email,id:user.id,fullName:user.fullname}
      const token = jwt.sign({...payload}, process.env.SECRET);
      res.status(200).json({
        message: "Logged in successfully",
        userData: {...payload},
        token
      });

}
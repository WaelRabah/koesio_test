const express =require('express')
const router = express.Router()
const usersController=require('../controllers/users.controller')
const messagesRouter=require('./messages.router')
router.get("/:id",usersController.getUserByID)
router.use('/:userId/messages',messagesRouter)


module.exports=router
const express = require('express');
const passport=require('passport')
const router = express.Router();
const authRouter=require('./auth.router')
const usersRouter=require('./users.router')

router.use('/',authRouter)
router.use('/users', passport.authenticate('jwt', {session: false}),usersRouter)
// router.use('/messages', passport.authenticate('jwt', {session: false}),messagesRouter)
router.get('/', (req, res) => {
  res.status(200).send({
    success: 'true',
    message: 'Express+PG messaging api',
    version: '1.0.0',
  });
});

module.exports = router;
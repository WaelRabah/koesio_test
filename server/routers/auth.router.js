const express =require('express')
const router = express.Router()
const authController=require('../controllers/auth.controller')
const { body } = require('express-validator');
router.post("/signup",  body('email').isEmail().withMessage('Invalid email'),body('password').isLength({ min: 8 }).withMessage('Password must be at least 8 chars long'),body('fullname').exists().isLength({ min: 1 }).withMessage('fullname can\'t be empty or null'),authController.signup)
router.post("/login",  body('email').isEmail().withMessage('Invalid email'),body('password').isLength({ min: 8 }).withMessage('Password must be at least 8 chars long'),authController.login)



module.exports=router
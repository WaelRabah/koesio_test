const express =require('express')
const router = express.Router({mergeParams:true})
const controller=require('../controllers/messages.controller')

router.get('/',controller.getMessagesByUser)
router.post('/',controller.sendMessage)

module.exports=router
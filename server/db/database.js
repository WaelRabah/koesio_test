const { Pool } = require('pg');
const dotenv = require('dotenv');

dotenv.config();
console.log(process.env.DATABASE_URL)
const pool = new Pool({
  connectionString: process.env.DATABASE_URL,
  
});

pool.on('connect', () => {
  console.log('Successful connection to the MessagingDB');
});

module.exports = {
  query: (text, params) => pool.query(text, params),
};
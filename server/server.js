const express= require("express")
const cors=require('cors')
const router=require('./routers')
const passport = require('passport')

const app = express()
app.use(express.json()); 
app.use(express.urlencoded({ extended: true }));   
// var corsOptions = {
//     origin: process.env.NODE_ENV==="development"? "http://localhost:3000/" : process.env.CLIENT_URL,
//     optionsSuccessStatus: 200,
//     credentials: true
//   }
app.use(cors());
app.use(passport.initialize())
require('./strategies/jwt')
app.use('/api',router)


app.get("/",(req,res)=>{
    
    res.status(200).send("Koesio test server is running on port 5000")
})


app.listen(process.env.PORT || 5000,()=>{
    console.log("Server successfully started!")
})
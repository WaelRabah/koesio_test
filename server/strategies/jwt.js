const passport = require('passport')    
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const db = require("../db/database");
const jwtOptions = {};
jwtOptions.jwtFromRequest=ExtractJwt.fromAuthHeaderAsBearerToken();
jwtOptions.secretOrKey = process.env.SECRET;

passport.use(new JwtStrategy(jwtOptions,async (jwt_payload,done) =>{
    const { rows } = await db.query(
        "SELECT * FROM USERS where id=$1",[jwt_payload.id]
      );
      if (rows.length){
        return done(null,rows[0]);
      }
      else {
        return done(null, false)
      }
    }
))
const {WebhookClient} = require("discord.js")
const { IncomingWebhook } = require('@slack/webhook');

module.exports.sendDiscordMsg=async (content,webhookUrl,fullname)=>{
    // const Hook = new discord_webhook.Webhook(webhookUrl);
    const webhookClient = new WebhookClient({ url: webhookUrl });

    const res=await webhookClient.send({
        content:content,
        username:fullname
    })
     return res 
}
module.exports.sendSlackMsg=async (content,webhookUrl,fullname)=>{
    const webhook = new IncomingWebhook(webhookUrl);
    const res=await webhook.send({
        text: content,
        username:fullname
      });
    return res
}
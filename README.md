# Koesio_Test

This is the repository for my submission to the Koesio technical test
demo_url : https://testkoesioclient.herokuapp.com/

### Justification des choix techniques 
<p>
J'ai choisi postgres comme BD car PG est donc plus efficace que MariaDB en ce qui concerne les lectures et les écritures, ce qui est important dans une application d'envoi de messages instantanés.
</p>
</br>
<p>J'ai choisi ExpressJS pour le backend car c'est un moyen rapide et fléxible pour configurer un serveur sécurisé et fiable.</p></br>
<p>J'ai choisi React car c'est bien adapté pour les petites applications et on écrit moins de codes par rapport à Angular et Vue pour avoir le même résultat.</p>

### 1-Install postgresql database, nodejs and npm versions for your OS

### 2-Create the database and tables using the sql commands in server/database_creation_commands.sql
### 3-Install the project dependencies for the client and server

```
cd client
npm i
cd ../server
npm i
```

### 4-setup environment variables
#### Server .env file :
Create a .env file in your server folder and add these env variables:
```
SECRET=YOUR_JWT_SECRET
DATABASE_URL=postgres://{user}:{password}@{host}:{port}/{DB}
```

#### Client .env file :
Create a .env file in your client folder and add these env variables:
```
REACT_APP_API_URL=http://localhost:5000/api
REACT_APP_PROD_API_URL=your prod link
```
### 5-run the backend server

```
cd server 
npm start
```

### 6-Start the client web app

```
cd client 
npm start
```



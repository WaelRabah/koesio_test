import AxiosInstance from './AxiosInstance'

export async function getMessages() {
    const userData = JSON.parse(localStorage.getItem('user_data'))
    const userId = userData['id']
    return await AxiosInstance({
        method: "GET",
        url: `users/${userId}/messages`,
        headers: {
            Accept: "application/json, text/plain, */*"
        }
    });
}

export async function sendMessage(messageData) {
    const userData = JSON.parse(localStorage.getItem('user_data'))
    const userId = userData['id']
    const body = { content: messageData['message'], platforms:[
        {
            name : "Discord",
            webhookUrl : messageData['discordWebhookUrl']
        },
        {
            name : "Slack",
            webhookUrl : messageData['slackWebhookUrl']
        }
    ] }
    return await AxiosInstance({
        method: "POST",
        url: `users/${userId}/messages`,
        data: body,
        headers: {
            Accept: "application/json, text/plain, */*"
        }
    });
}
import AxiosInstance from './AxiosInstance'

export async function getUserData(){
    const userData=JSON.parse(localStorage.getItem('user_data'))
    const userId=userData['id']
    return await AxiosInstance({
        method: "GET",
        url: `users/${userId}`,
        headers: {
          Accept: "application/json, text/plain, */*"
        }
      });
}
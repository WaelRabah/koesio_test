import axios from "axios";

let instance


instance = axios.create({});

instance.interceptors.request.use(
  function (config) {
    const token=localStorage.getItem('jwt_token')
    config["baseURL"] = process.env.NODE_ENV==="development" ? process.env.REACT_APP_DEV_API_URL : process.env.REACT_APP_PROD_API_URL 
    config.headers.Authorization =  token ? `Bearer ${token}` : '';
    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);

export default instance;
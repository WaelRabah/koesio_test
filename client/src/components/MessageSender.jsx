import React, { useState, useEffect } from 'react'
import { Card, Button, Form } from 'react-bootstrap';


function MessageSender({ sendMessage }) {
    const [formData, setFormData] = useState({
        message: "",
        discordWebhookUrl: "",
        slackWebhookUrl: ""
    })
    const handleChange = (e) => {
        const { name, value } = e.target
        setFormData({ ...formData, [name]: value })
    }
    useEffect(() => {
        const discordWH = localStorage.getItem('discordWebhook')
        const slackWH = localStorage.getItem('slackWebhook')
        setFormData({ ...formData, discordWebhookUrl: discordWH || "", slackWebhookUrl: slackWH || "" })
    }, [])

    return (
        <Card style={{ height: "100%", width: "100%", paddingTop: "8vh" }}>
            <div style={{ alignItems: "center", justifyContent: "center", display: "flex", flexWrap: "wrap" }}>
                <Form style={{ width: "80%", height: "100%" }} >

                    <Form.Group className="mb-3" controlId="sendMsg.msg">
                        <Form.Control as="textarea" onChange={(e) => { setFormData({ ...formData, message: e.target.value }) }} value={formData.message} rows={8} style={{ width: "100%" }} />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="sendMsg.discord">
                        <Form.Control type="text" name="discordWebhookUrl" value={formData['discordWebhookUrl']} onChange={handleChange} placeholder="Discord webhook url..." />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="sendMsg.slack">
                        <Form.Control type="text" name="slackWebhookUrl" value={formData['slackWebhookUrl']} onChange={handleChange} placeholder="Slack webhook url..." />
                    </Form.Group>
                </Form>
                <div
                    style={{ width: "50%", display: "flex", justifyContent: "space-between" }}
                ><Button variant="dark" onClick={() => {
                    if (formData['message'].length === 0) {
                        return
                    }
                    sendMessage(formData)
                    setFormData({ ...formData, message: '' })

                }}>Send Message</Button>
                    <Button variant="dark" onClick={() => {
                        if (formData['discordWebhookUrl'].length) {
                            localStorage.setItem('discordWebhook', formData['discordWebhookUrl'])
                        }
                        if (formData['slackWebhookUrl'].length) {
                            localStorage.setItem('slackWebhook', formData['slackWebhookUrl'])
                        }
                    }}>Save webhook urls</Button>
                </div>
            </div>
        </Card>
    )
}

export default MessageSender
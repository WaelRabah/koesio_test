import React from 'react'
import MessageCard from './MessageCard';
import { Card } from 'react-bootstrap';
function MessagesList({messages}) {

    return (
        <Card style={{
            minHeight: "60vh",
            maxHeight: "60vh",
            minWidth:"100%",
            overflowY:"auto",
            padding:5,
            minWidth:"40vw",
            alignItems : "center"
        }}>
            
            { messages.length ? (<div>
                {
                    messages.map(({content, date, platforms, status},index)=>(
                        <div style={{ height: "auto",marginBottom:5}} key={index.toString()}>
                        <MessageCard content={content} platforms={platforms} date={date} status={status} />
                        </div>
                    ))
                }
            </div>) : (
                <div style={{height : "100%", fontWeight :"bold",marginTop:20}}>
                    0 messages were sent
                </div>
            )
                
            }
            

            <div style={{height:20,width:20}}></div>
        </Card>
    )
}

export default MessagesList
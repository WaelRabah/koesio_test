import React from 'react'
import {  Card } from 'react-bootstrap';
import PlatformStatus from './PlatformStatus';
function MessageCard({content, platforms, date, status}) {
  return (
    <Card  style={{ paddingLeft:40,minWidth:"40vw",height:"auto"}}>
      {/* <Card.Img variant="top" src="holder.js/100px180" /> */}
      <Card.Body>
        <div style={{display:"flex",justifyContent:"space-between"}}>
        <div>
        <Card.Text>
          {content}
        </Card.Text>
        </div>
        <div style={{display: "flex", justifyContent:"flex-start", flexWrap:"wrap"}}>
                {
                    platforms.map(({name,status},index)=><div style={{marginLeft: 4}} key={index.toString()}><PlatformStatus name={name} status={status}  /></div>)
                }
  

        </div>
        </div>
      </Card.Body>
    </Card>
  )
}

export default MessageCard
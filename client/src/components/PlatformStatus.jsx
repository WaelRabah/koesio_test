import React from 'react'
import {Badge } from 'react-bootstrap';
function PlatformStatus({name, status}) {
  return (
        <Badge bg={status ? "success" : "danger"}>{name}</Badge>
  )
}

export default PlatformStatus
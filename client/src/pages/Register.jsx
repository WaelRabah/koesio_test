import React, { useState, useEffect } from 'react'
import { Card, Button, Form, Container } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import { register } from '../services/auth.service';
function Register() {
    const navigate = useNavigate();
    useEffect(() => {

        const token=localStorage.getItem('jwt_token')
        if (token){
          navigate('/')
        }
      
      }, [])
    const [formData, setFormData] = useState({
        email: "",
        password: "",
        fullname: ""
    })
    
    const [validationErrors, setValidationErrors] = useState({
        email: false,
        password: false,
        fullname: false
    })
    const [isValid, setIsValid] = useState(true)
    const [validationString, setValidationString] = useState("")
    const handleChange = (e) => {
        const { name, value } = e.target
        checkValidation(name, value)
        setFormData({ ...formData, [e.target.name]: e.target.value })
    }
    const checkValidation = (name, value) => {
        setIsValid(true)
        if (name == 'email') {
            const checkValid = !value.includes("@") || !value.includes(".")
            setIsValid(!checkValid)
            setValidationErrors({ ...validationErrors, [name]: checkValid })
        }
        if (name == 'password') {
            const checkValid = !(value.length >= 8)
            setIsValid(!checkValid)
            setValidationErrors({ ...validationErrors, [name]: checkValid })
        }
        if (name == 'fullname') {
            const checkValid = !(value.length > 0)
            setIsValid(!checkValid)
            setValidationErrors({ ...validationErrors, [name]: checkValid })
        }
    }
    const handleSubmit =async () => {
        if (!isValid){
            return;
        }
        
        const res=await register(formData.fullname, formData.email, formData.password)
        
        if (res.status===302 || res.response && res.response.status){
            setIsValid(false)
            setValidationString(res.response.data.message)
            return
        }
        if (res.status===201){
            navigate('/login')
            return
        }
        
    }
    const handleReset = () => {
        setFormData({
            email: "",
            password: "",
            fullname: ""
        })
    }
    return (
        <Container className="container">
            <Card style={{ height: "auto", width: "60%", alignItems: "center", paddingTop: "5%", paddingBottom: "5%" }}>
                <div style={{ width: "80%", height: "auto" }}>
                    <Form.Group className="mb-3" controlId="loginForm.fullname">
                        <Form.Label>Full name</Form.Label>
                        <Form.Control type="text" name="fullname" value={formData['fullname']} onChange={handleChange} placeholder="fullname..." />
                        <div style={{color : "#ff0f0f", fontSize:11,paddingTop:4}} >{ validationErrors['fullname'] ? "Invalid full name" : ""}</div>

                    </Form.Group>
                    <Form.Group className="mb-3" controlId="loginForm.email">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control type="email" name="email" value={formData['email']} onChange={handleChange} placeholder="name@example.com" />
                        <div style={{color : "#ff0f0f", fontSize:11,paddingTop:4}}>{ validationErrors['email'] ? "Invalid email" : ""}</div>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="loginForm.password">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" name="password" value={formData['password']} onChange={handleChange} placeholder="password..." />
                        <div style={{color : "#ff0f0f", fontSize:11,paddingTop:4}}>{ validationErrors['password'] ? "Invalid password" : ""}</div>
                    </Form.Group>
                    <div>
                    <Button style={{ margin: 4 }} onClick={handleSubmit} >Submit</Button>
                    <Button style={{ margin: 4 }} variant="light" onClick={handleReset}>Reset</Button>
                    <Button style={{margin:4}} variant="secondary"  onClick={()=>{
                    navigate('/login')
                }}>Login</Button>
                </div>
                <div style={{color : "#ff0f0f", fontSize:11,paddingTop:4, alignSelf:"flex-start"}}>
                { !isValid ? validationString || "Invalid form data!" : ""}
                </div>
                </div>

            </Card>

        </Container>
    )
}

export default Register
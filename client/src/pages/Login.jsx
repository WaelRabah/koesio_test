import React, {useState, useEffect} from 'react'
import { Card, Button, Form, Container} from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import { login } from '../services/auth.service';
function Login() {
    const navigate = useNavigate();
    useEffect(() => {

        const token=localStorage.getItem('jwt_token')
        if (token){
          navigate('/')
        }
      
      }, [])
    const [formData,setFormData]=useState({
        email:"",
        password:""
    })
    const [validationErrors, setValidationErrors] = useState({
        email: false,
        password: false,
    })
    const [isValid, setIsValid] = useState(true)
    const [validationString, setValidationString] = useState("")
    const handleChange = (e) => {
        const { name, value } = e.target
        checkValidation(name, value)
        setFormData({ ...formData, [e.target.name]: e.target.value })
    }
    const checkValidation = (name, value) => {
        setIsValid(true)
        if (name == 'email') {
            const checkValid = !value.includes("@") || !value.includes(".")
            setIsValid(!checkValid)
            setValidationErrors({ ...validationErrors, [name]: checkValid })
        }
        if (name == 'password') {
            const checkValid = !(value.length >= 8)
            setIsValid(!checkValid)
            setValidationErrors({ ...validationErrors, [name]: checkValid })
        }

    }
    const handleSubmit =async () => {
        if (!isValid){
            return;
        }
        
        const res=await login(formData.email,formData.password)

        if (res.status===404){
            setIsValid(false)
            setValidationString(res.data.message)
            return
        }
        if (res.status===200){
            const token= res.data.token
            const userData=res.data.userData
            localStorage.setItem('jwt_token',token)
            localStorage.setItem('user_data',JSON.stringify(userData))
            navigate('/')
            return
        }
        
    }
    const handleReset=()=>{
        setFormData({
            email: "",
            password: ""
        })
    }
    return (
        <Container className="container">
            <Card style={{ height: "auto", width: "60%", alignItems: "center", paddingTop: "5%",paddingBottom: "5%" }}>
                <div style={{width:"80%",height:"auto"}}>
                <Form.Group className="mb-3" controlId="loginForm.email">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" onChange={handleChange} value={formData['email']}  name="email" placeholder="name@example.com" />
                    <div style={{color : "#ff0f0f", fontSize:11,paddingTop:4}}>{ validationErrors['email'] ? "Invalid email" : ""}</div>
                </Form.Group>
                <Form.Group className="mb-3" controlId="loginForm.password">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" onChange={handleChange} value={formData['password']} name="password" placeholder="name@example.com" />
                    <div style={{color : "#ff0f0f", fontSize:11,paddingTop:4}}>{ validationErrors['password'] ? "Invalid password" : ""}</div>
                </Form.Group>
                <div>
                
                <Button style={{margin:4}}  onClick={handleSubmit} disabled={!isValid} >Submit</Button>
                <Button style={{margin:4}} variant="light" onClick={handleReset}>Reset</Button>
                <Button style={{margin:4}} variant="secondary"  onClick={()=>{
                    navigate('/register')
                }}>Register</Button>
                </div>

                <div style={{color : "#ff0f0f", fontSize:11,paddingTop:4, alignSelf:"flex-start"}}>
                    { !isValid ? validationString || "Invalid form data!" : ""}
                </div>
                </div>
 
            </Card>

        </Container>
    )
}

export default Login
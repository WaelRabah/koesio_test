import React, { useEffect, useState } from 'react'
import { Container } from 'react-bootstrap';
import MessagesList from '../components/MessagesList';
import MessageSender from '../components/MessageSender';
import { useNavigate } from 'react-router-dom';
import { getMessages, sendMessage } from '../services/messages.service';
function Messages() {
  const navigate = useNavigate();
  useEffect(() => {

    const token = localStorage.getItem('jwt_token')
    if (!token) {
      navigate('/login')
      return;
    }
    (async () => {
      await fetchMessages()

    })()
  }, [])
  const [messages, setMessages] = useState([
  ])
  const fetchMessages = async () => {
    const msgs = await getMessages()

    setMessages(msgs.data.results)
  }


  const send = async (message) => {
    const res = await sendMessage(message, [
      {
        name: "Discord",
        status: true
      },
      {
        name: "Slack",
        status: true
      }
    ])
    if (res.status === 200) {
      await fetchMessages()
    }
  }
  return (
    <Container className="container">
      <div className='side-by-side-container'>
        <div className='message-sender'>
          <MessageSender sendMessage={send} />
        </div>
        <div className='separator'></div>
        <div className='messages-list'>
          <MessagesList messages={messages} />
        </div>


      </div>

    </Container>
  )
}

export default Messages
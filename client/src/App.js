import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import Login from './pages/Login';
import {
  BrowserRouter as Router,
  Routes,
  Route,
} from "react-router-dom"; 
import Messages from './pages/Messages';
import Register from './pages/Register';
function App() {
  return (
    <div style={{backgroundColor:"#7289da"}}>
    <Router initial="/">
      <Routes >
        <Route path="/"  element={<Messages />} />
        <Route path="/register" element={<Register />} />
        <Route path="/login"  element={<Login />}/>
      </Routes>
    </Router>
    </div>
  );
}

export default App;
